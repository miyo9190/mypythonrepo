import random

playerHp = 250
enemyAtkL = 50
enemyAtkH = 80

while playerHp > 0:
    dmg = random.randrange(enemyAtkL, enemyAtkH)
    playerHp = playerHp - dmg

    if playerHp <= 30:
        playerHp = 30

    print('Enemy hits for', dmg, ' points of damage. HP left: ', playerHp)

    if playerHp > 30:
        # pass # do nothing
        continue # ignores next and does next iteration

    print('Low health, do yo have any potions? Or food?')
    break
