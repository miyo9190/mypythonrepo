import re

print('Calculator powa')
print('Type "quit" to exit')

previous = 0
run = True


def performMath():
    global run  # to access the variable
    global previous
    if previous == 0:
        equation = input('Enter equation: ')
    else:
        equation = input(str(previous))
    if equation == 'quit':
        print('Bye bye')
        run = False
        print('Hello')
    else:
        equation = re.sub('[a-zA-z,.:()" "]', '', equation)
        if previous == 0:
            previous = eval(equation)
        else:
            previous = eval(str(previous) + equation)


while run:
    performMath()
