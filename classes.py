class Enemy:
    hp = 200

    def __init__(self, atkL, atkH): # initialization of variables (dynamic)
        self.atkL = atkL
        self.atkH = atkH

    def getAtk(self):
        print("Attack:", self.atkL)

    def getHp(self):
        print("Hp:", self.hp)


enemy1 = Enemy(40, 50)
enemy1.getAtk()
enemy1.getHp()
enemy2 = Enemy(90, 100)
enemy2.getAtk()
